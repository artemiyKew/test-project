// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'injector.dart';

// **************************************************************************
// KiwiInjectorGenerator
// **************************************************************************

class _$Injector extends Injector {
  @override
  void _configure() {
    final KiwiContainer container = KiwiContainer();
    container
      ..registerFactory<Datasource>((c) => RemoteDatasourceImpl())
      ..registerFactory<GitInfoRepository>(
          (c) => GitInfoRepositoryImpl(c<Datasource>()))
      ..registerFactory((c) => GetGitInfo(c<GitInfoRepository>()))
      ..registerFactory((c) => MainPageCubit(c<GetGitInfo>()));
  }
}
