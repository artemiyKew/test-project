import 'package:kiwi/kiwi.dart';
import 'package:mintrocket/features/data/datasource/datasource.dart';
import 'package:mintrocket/features/data/datasource/remote_datasource_impl.dart';
import 'package:mintrocket/features/data/repositories/git_info_repository_impl.dart';
import 'package:mintrocket/features/domain/repositories/git_info_repository.dart';
import 'package:mintrocket/features/domain/usecases/get_git_info.dart';
import 'package:mintrocket/features/presentation/main_page/controllers/main_page_cubit.dart';

part 'injector.g.dart';

abstract class Injector {
  static KiwiContainer? kiwiContainer;

  static void setup() {
    kiwiContainer = KiwiContainer();
    _$Injector()._configure();
  }

  static final resolve = kiwiContainer!.resolve;
  // DataSource
  @Register.factory(Datasource, from: RemoteDatasourceImpl)
  // Repos
  @Register.factory(GitInfoRepository, from: GitInfoRepositoryImpl)
  // Usecases
  @Register.factory(GetGitInfo)
  // Blocs/Cubit
  @Register.factory(MainPageCubit)
  void _configure();
}
