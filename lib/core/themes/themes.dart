import 'dart:ui';

import 'package:flutter/material.dart';

part 'values.dart';


class Themes {
  static ThemeData get lightTheme =>
      ThemeData(
        brightness: Brightness.light,
        scaffoldBackgroundColor: _Values.white,
        primaryColor: _Values.violet,
        primaryColorDark: _Values.darkViolet,
        primaryColorLight: _Values.lightViolet,
      );

  static ThemeData get darkTheme => lightTheme;
}
