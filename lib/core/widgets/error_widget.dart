import 'package:flutter/material.dart';
import 'package:mintrocket/core/error/failure.dart';
import 'package:mintrocket/core/extension/theme_context.dart';

class ErrorWidget extends StatelessWidget {
  final Failure _failure;

  const ErrorWidget(this._failure, {Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        _failure.msg,
        style: context.theme.textTheme.headline3!.copyWith(
          color: Colors.red.shade800,
        ),
      ),
    );
  }
}
