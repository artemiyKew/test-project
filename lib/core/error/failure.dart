import 'package:equatable/equatable.dart';

class Failure extends Equatable {
  final String msg;

  Failure(this.msg);

  @override
  String toString() {
    return this.msg;
  }

  @override
  List<Object?> get props => [msg];
}

// General failures
class ServerFailure extends Failure {
  ServerFailure(String msg) : super(msg);

  @override
  List<Object?> get props => [msg];
}

class CacheFailure extends Failure {
  CacheFailure(String msg) : super(msg);

  @override
  List<Object?> get props => [msg];
}
