import 'package:dartz/dartz.dart';
import 'package:mintrocket/core/error/failure.dart';
import 'package:mintrocket/features/domain/entities/git_info.dart';

abstract class GitInfoRepository {
  Future<Either<Failure, List<GitInfo>>> getGitInfo();
}
