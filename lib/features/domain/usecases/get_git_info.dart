import 'package:dartz/dartz.dart';
import 'package:mintrocket/core/error/failure.dart';
import 'package:mintrocket/core/usecases/usecases.dart';
import 'package:mintrocket/features/domain/entities/git_info.dart';
import 'package:mintrocket/features/domain/repositories/git_info_repository.dart';

class GetGitInfo extends UseCase<List<GitInfo>, NoParams> {
  final GitInfoRepository _repository;

  GetGitInfo(this._repository);

  @override
  Future<Either<Failure, List<GitInfo>>> call(NoParams params) {
    return _repository.getGitInfo();
  }
}
