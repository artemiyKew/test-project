import 'package:equatable/equatable.dart';

class GitInfo extends Equatable {
  final int id;
  final String name;
  final String ownerLogin;
  final String? description;
  final String gitUrl;

  GitInfo({
    required this.id,
    required this.name,
    required this.ownerLogin,
    required this.description,
    required this.gitUrl,
  });

  @override
  List<Object?> get props => [id, name, ownerLogin, description, gitUrl];
}
