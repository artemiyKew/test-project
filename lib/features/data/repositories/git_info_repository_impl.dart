import 'package:dartz/dartz.dart';
import 'package:mintrocket/core/error/exceptions.dart';
import 'package:mintrocket/core/error/failure.dart';
import 'package:mintrocket/features/data/datasource/datasource.dart';
import 'package:mintrocket/features/domain/entities/git_info.dart';
import 'package:mintrocket/features/domain/repositories/git_info_repository.dart';

class GitInfoRepositoryImpl implements GitInfoRepository {
  final Datasource _datasource;

  GitInfoRepositoryImpl(
    this._datasource,
  );
  @override
  Future<Either<Failure, List<GitInfo>>> getGitInfo() async {
    try {
      final List<GitInfo> remoteGitInfo = await _datasource.getGitInfo();
      return Right(remoteGitInfo);
    } on ServerException {
      return Left(
          ServerFailure('Проблема с сервером. Не удается получить данные!'));
    }
  }
}
