import 'package:mintrocket/features/domain/entities/git_info.dart';

class GitInfoModel extends GitInfo {
  GitInfoModel({
    required int id,
    required String name,
    required String ownerLogin,
    required String? description,
    required String gitUrl,
  }) : super(
          id: id,
          name: name,
          ownerLogin: ownerLogin,
          description: description ?? 'Описания нет',
          gitUrl: gitUrl,
        );

  factory GitInfoModel.fromJson(Map<String, dynamic> json) {
    return GitInfoModel(
      id: json['id'],
      name: json['name'],
      ownerLogin: json['owner']['login'],
      description: json['description'],
      gitUrl: json['html_url'],
    );
  }

  Map<String, dynamic> toJson() {
    return {
      'id': id,
      'name': name,
      'ownerLogin': ownerLogin,
      'description': description,
      'gitUrl': gitUrl,
    };
  }
}
