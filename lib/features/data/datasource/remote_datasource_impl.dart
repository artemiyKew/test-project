import 'package:mintrocket/core/error/exceptions.dart';
import 'package:mintrocket/features/data/datasource/datasource.dart';
import 'package:mintrocket/features/data/models/git_info_model.dart';
import 'package:mintrocket/features/domain/entities/git_info.dart';

import 'package:dio/dio.dart' as dio;


class RemoteDatasourceImpl implements Datasource {
  final String url = 'https://api.github.com/orgs/mintrocket/repos';
  @override
  Future<List<GitInfo>> getGitInfo() async {
    final response = await dio.Dio().get(url);
    if (response.statusCode == 200) {
      return response.data.map<GitInfoModel>((e) => GitInfoModel.fromJson(e)).toList();
    } else {
      throw ServerException();
    }
  }
}
