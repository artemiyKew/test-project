import 'package:mintrocket/features/domain/entities/git_info.dart';

abstract class Datasource {
  Future<List<GitInfo>> getGitInfo();
}