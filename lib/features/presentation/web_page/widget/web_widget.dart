import 'package:flutter/material.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebWidget extends StatelessWidget {
  const WebWidget({ Key? key, required this.urlPage }) : super(key: key);
  final String urlPage;
  @override
  Widget build(BuildContext context) {
    return WebView(
      initialUrl: urlPage,
    );
  }
}