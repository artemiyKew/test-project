import 'package:flutter/material.dart';
import 'package:mintrocket/core/extension/theme_context.dart';
import 'package:mintrocket/features/domain/entities/git_info.dart';
import 'package:mintrocket/features/presentation/web_page/widget/web_widget.dart';
import 'package:share_plus/share_plus.dart';

class WebViewPage extends StatelessWidget {
  const WebViewPage({Key? key, required this.gitInfo}) : super(key: key);
  final GitInfo gitInfo;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        backgroundColor: context.theme.scaffoldBackgroundColor,
        appBar: AppBar(
          title: Text('${gitInfo.name}'),
          actions: [
            IconButton(
              icon: Icon(Icons.share),
              onPressed: () {
                share(context, gitInfo);
              },
            ),
          ],
        ),
        body: WebWidget(urlPage: gitInfo.gitUrl));
  }

  void share(BuildContext context, GitInfo gitInfo) {
    final RenderBox box = context.findRenderObject() as RenderBox;
    final String text = 'Project link: ${gitInfo.gitUrl}';
    Share.share(text, subject: gitInfo.gitUrl, sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
  }
}
