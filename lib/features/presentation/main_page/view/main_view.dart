import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mintrocket/core/di/injector.dart';
import 'package:mintrocket/core/extension/theme_context.dart';
import 'package:mintrocket/features/presentation/main_page/controllers/main_page_cubit.dart';
import 'package:mintrocket/features/presentation/main_page/controllers/main_page_state.dart';
import 'package:mintrocket/core/widgets/index.dart' as core_widgets;
import 'package:mintrocket/features/presentation/main_page/widget/main_widget.dart';

class MainViewPage extends StatelessWidget {
  const MainViewPage({Key? key}) : super(key: key);

  Widget _buildBody(BuildContext context) {
    return BlocBuilder<MainPageCubit, MainPageState>(builder: (context, state) {
      return state.when(
          loading: () => const core_widgets.LoadingWidget(),
          success: (gitInfo) => MainWidget(gitInfo: gitInfo),
          error: (failure) => core_widgets.ErrorWidget(failure));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      backgroundColor: context.theme.scaffoldBackgroundColor,
      appBar: AppBar(
        title: Text('Git projects'),
      ),
      body: SafeArea(
        child: BlocProvider(
          create: (context) => Injector.resolve<MainPageCubit>()..init(),
          child: _buildBody(context),
        ),
      ),
    );
  }
}
