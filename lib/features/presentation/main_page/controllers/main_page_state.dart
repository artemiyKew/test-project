import 'package:freezed_annotation/freezed_annotation.dart';
import 'package:mintrocket/core/error/failure.dart';
import 'package:mintrocket/features/domain/entities/git_info.dart';

part 'main_page_state.freezed.dart';

@freezed
class MainPageState with _$MainPageState {
  const factory MainPageState.loading() = Loading;
  const factory MainPageState.success(List<GitInfo> gitInfoModel) = Success;
  const factory MainPageState.error(Failure failure) = Error;
}

