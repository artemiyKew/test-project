import 'package:bloc/bloc.dart';
import 'package:dartz/dartz.dart';
import 'package:mintrocket/core/error/failure.dart';
import 'package:mintrocket/core/usecases/usecases.dart';
import 'package:mintrocket/features/domain/entities/git_info.dart';
import 'package:mintrocket/features/domain/usecases/get_git_info.dart';
import 'main_page_state.dart';

class MainPageCubit extends Cubit<MainPageState> {
  MainPageCubit(this.getGitInfo) : super(Loading());
  final GetGitInfo getGitInfo;

  Future<void> init() async {
    await gitInfo();
  }

  Future<void> gitInfo() async {
    final Either<Failure, List<GitInfo>> failureOrGit =
        await getGitInfo(NoParams());
    failureOrGit.fold((failure) => emit(Error(failure)), (gitInfo) => emit(Success(gitInfo)));
  }
}
