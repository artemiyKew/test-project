import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:mintrocket/features/domain/entities/git_info.dart';
import 'package:mintrocket/features/presentation/main_page/controllers/main_page_cubit.dart';
import 'package:mintrocket/features/presentation/web_page/view/web_view.dart';

class MainWidget extends StatelessWidget {
  final List<GitInfo> gitInfo;
  const MainWidget({Key? key, required this.gitInfo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
      onRefresh: () => BlocProvider.of<MainPageCubit>(context).gitInfo(),
      child: ListView.builder(
        itemCount: gitInfo.length,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => WebViewPage(
                          gitInfo: gitInfo[index],
                        )),
              );
            },
            child: Card(
              clipBehavior: Clip.antiAlias,
              child: Column(
                children: [
                  ListTile(
                    title: Text(
                        'Project name: ${gitInfo[index].name} (${gitInfo[index].id})'),
                    subtitle: Text('Owner Login: ${gitInfo[index].ownerLogin}'),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(16.0),
                    child: Text('${gitInfo[index].description}'),
                  )
                ],
              ),
            ),
          );
        },
      ),
    );
  }
}
