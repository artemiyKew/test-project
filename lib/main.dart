import 'package:flutter/material.dart';
import 'package:mintrocket/core/di/injector.dart';
import 'package:mintrocket/core/themes/themes.dart';
import 'package:mintrocket/features/presentation/main_page/view/main_view.dart';

void main() async {
  Injector.setup();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: Themes.lightTheme,
      title: 'Flutter Demo',
      debugShowCheckedModeBanner: false,
      home: MainViewPage(),
    );
  }
}
